'use babel';
import GitflowToolbarView from './view.toolbar';


export default class DomQuery {

    construct(q='*') {
        this.collection = []
        if (q.startsWith('.'))
            this.collection = this.clsQuery(q);
        else if (q.startsWith('#')) {
            this.collection = this.idQuery(q);
        }
    }

    clsQuery(q='*') {
        if (q.startsWith('.')) q = q.substring(1);
        return document.getElementsByClassName(q);
    }

    idQuery(q='*') {
        if (q.startsWith('#')) q = q.substring(1);
        return document.getElementById(q);
    }

    __noSuchMethod__ (id, args) {
        console.log(id);
        console.log(args);
    }

}


export default class GuiElement {
}

/*
export default class GuiObject {

    construct(elem, parent=null, classes=[]) {
        // if(!elem) elem = new GuiElement();
        elem = elem ? elem : document.createElement(elem);
        this.element = elem;
        this.element.setAttribute('class', classes.join(" "));
        if (parent) parent.appendChild(this.element);
    }

    get() {
        return this.element;
    }

    cls(classes=[]) {
        classes.push(this.element.getAttribute('class'));
        this.element.setAttribute('class', classes.join(" "));
        return this.element.getAttribute('class');
    }

    content(text="") {
        if (text) this.element.textContent = text;
        return text;
    }

    parent(newParent=null) {
        if (newParent) newParent.appendChild(this.element);
        return this.element.parent;
    }

    children(chlrn=[]) {
        for (var child in chlrn)
            this.element.appendChild(child);
        return this.element.children;
    }

}
*/


export default class Notifications {

    construct() {
        this.manager = atom.notifications;
    }

    info(message, details="", icon='circle-slash', dismissable=false) {
        this.manager.addInfo(message, {
            detail: details,
            dismissable: dismissable,
            icon: icon
        });
        return false;
    }

}
