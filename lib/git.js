'use babel';
import Notifications from './util';
import {BufferedProcess, process} from 'atom';
//import Gitflow from './git-flow';


export default class GitHelper {

    construct() {
        this.notifications = new Notifications();
        this.options = {
            cwd: atom.project.getPaths()[0],
            env: process.env
        };
        this.ws = atom.views.getView(atom.workspace);
    }

    cmd(command=null, args=[]) {
        if (!command) {
            this.notifications.info("No command specified");
            return;
        }
        command = "git " + command;
        var process = new BufferedProcess({
            command: command,
            args: args,
            options: this.options,
            stdout: console.log,
            stderror: console.log});
        process.onWillThrowError = function(e) {
            console.log(e);
        };
    }

    add() {
        atom.commands.dispatch('atom-workspace', 'git-plus:add');
    }

    commit() {
        atom.commands.dispatch('atom-workspace', 'git-plus:commit');
    }

}
