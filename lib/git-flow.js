'use babel';

import GitflowToolbarView from './view.toolbar';
import GitHelper from './git';
import { CompositeDisposable } from 'atom';

export default {

    toolbar: null,
    panel: null,
    subscriptions: null,
    config: {
        showOnActivation: {
            type: 'boolean',
            default: true
        },
        toolbarPosition: {

            type: 'string',
            default: 'top'
        }
    },

    activate(state) {
        this.toolbar = new GitflowToolbarView();
        this.toolbar.panel.show();
        this.git = new GitHelper();
        this.git.cmd('status');
        window.G = this;

        var self = this;
        this.subscriptions = new CompositeDisposable();
        this.subscriptions.add(atom.commands.add('atom-workspace', {
            'git-flow:toggle': () => self.toolbar.toggle(),
            'git-flow:add': () => self.git.add(),
            'git-flow:commit': () => self.git.commit(),
        }));

        this.setupAction(self.git.add, {
            text: 'Add current',
            icon: 'plus',
        });
    },


    deactivate() {
        this.toolbar.panel.destroy();
        this.subscriptions.dispose();
        this.toolbar.destroy();
    },


    setupAction(action, options) {
        if(!action) return false;
        var btn = this.toolbar.addActionButton(options);
        btn.addEventListener('click', function() {
            action();
        });
    },


    serialize() {
        return {
            //gitFlowState: this.toolbar.serialize()
        };
    },


};
